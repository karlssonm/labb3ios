//
//  MyTableView.m
//  ToDo
//
//  Created by Mattias k on 2015-02-20.
//  Copyright (c) 2015 Mattias k. All rights reserved.
//

#import "MyTableView.h"
#import "MyDetailView.h"
#import "AddTaskView.h"
#import "Task.h"

@interface MyTableView ()


@property (nonatomic) NSMutableArray *tasks;
@property (nonatomic) Task *task;
@property (nonatomic) NSString *info;
@end

@implementation MyTableView

-(NSMutableArray *)tasks {
    if(!_tasks){
    
        Task *task1 = [[Task alloc]init];
        task1.name = @"städa";
        task1.info = @"nedre våningen behöver dammsugas och fönster putsas";
        Task *task2 = [[Task alloc]init];
        task2.name = @"handla";
        task2.info = @"Mjölk, skinka ketchup";
        Task *task3 = [[Task alloc]init];
        task3.name = @"tanka";
        task3.info = @"nere på reservtanken";
        
        _tasks = [@[task1, task2, task3] mutableCopy];
        
    }
    return _tasks;
}

-(void)viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tasks.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCell" forIndexPath:indexPath];
        self.task = [self.tasks objectAtIndex:indexPath.row];
        cell.textLabel.text = self.task.name;
        self.info = self.task.info;
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
        [self.tasks removeObjectAtIndex:indexPath.row];
        [tableView reloadData];
 }


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"Detail"]) {
        MyDetailView *detailView = [segue destinationViewController];
        UITableViewCell *cell = sender;
        detailView.title = cell.textLabel.text;
        detailView.task = [self.tasks objectAtIndex:[[self.tableView indexPathForSelectedRow] row]];
    }
    else if([segue.identifier isEqualToString:@"Add"]) {
        AddTaskView *addView = [segue destinationViewController];
        addView.tasks = self.tasks;
    }
    else {
        NSLog(@"You forgot the segue %@",segue);
    }
}


@end
