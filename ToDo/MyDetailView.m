//
//  MyDetailView.m
//  ToDo
//
//  Created by Mattias k on 2015-02-20.
//  Copyright (c) 2015 Mattias k. All rights reserved.
//

#import "MyDetailView.h"

@interface MyDetailView ()
@property (weak, nonatomic) IBOutlet UITextView *infoText;

@end

@implementation MyDetailView

- (void)viewDidLoad {
    [super viewDidLoad];
    self.infoText.text = self.task.info;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
