//
//  AddTaskView.h
//  ToDo
//
//  Created by Mattias k on 2015-02-20.
//  Copyright (c) 2015 Mattias k. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AddTaskView : UIViewController

@property (nonatomic) NSMutableArray *tasks;
@end
