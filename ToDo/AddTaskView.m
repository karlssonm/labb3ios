//
//  AddTaskView.m
//  ToDo
//
//  Created by Mattias k on 2015-02-20.
//  Copyright (c) 2015 Mattias k. All rights reserved.
//

#import "AddTaskView.h"
#import "Task.h"

@interface AddTaskView ()
@property (weak, nonatomic) IBOutlet UITextField *taskName;
@property (weak, nonatomic) IBOutlet UITextView *taskInfo;
@property (nonatomic) Task *task;

@end

@implementation AddTaskView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)addTask:(id)sender {
    
    self.task = [[Task alloc]init];
    self.task.name = self.taskName.text;
    self.task.info = self.taskInfo.text;
    [self.tasks addObject:self.task];
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
